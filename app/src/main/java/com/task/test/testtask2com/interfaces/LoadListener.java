package com.task.test.testtask2com.interfaces;

/**
 * Created by user on 24.12.2015.
 * for activity make changes after load is finish
 */
public interface LoadListener {
    void isLoad(boolean isNoError);
}
