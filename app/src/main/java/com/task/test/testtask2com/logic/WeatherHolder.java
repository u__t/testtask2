package com.task.test.testtask2com.logic;

import android.content.Context;

import com.task.test.testtask2com.interfaces.LoadListener;

/**
 * Created by user on 24.12.2015.
 *
 */
public class WeatherHolder {
    private static WeatherHolder mInstance = null;
    private boolean mIsLoad;
    private WeatherItem[] mWeatherItems;
    private LoadListener mDelegate;
    private WeatherHolder(){
        mIsLoad = false;
    };
    static public WeatherHolder getInstance(){
        if(null == mInstance){
            mInstance = new WeatherHolder();
        }
        return mInstance;
    }
    public void loadWeather(Context _context){
        if(!mIsLoad){
            mIsLoad = true;
            LoadClass loadObj = new LoadClass(_context);
            loadObj.execute();
        }
    }

    public void loadFinish(WeatherItem[] _items){
        mIsLoad = false;
        if(null != _items){
            setWeatherItems(_items);
            mDelegate.isLoad(true);
        }else
            mDelegate.isLoad(false);
    }
    public void setWeatherItems(WeatherItem[] _items){
        mWeatherItems = _items;
    }
    public WeatherItem[] getWeatherItems(){
        return mWeatherItems;
    }
    public void setDelegate(LoadListener _delegate){
        mDelegate = _delegate;
    }
}
