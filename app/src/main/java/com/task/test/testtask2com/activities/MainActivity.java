package com.task.test.testtask2com.activities;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.task.test.testtask2com.R;
import com.task.test.testtask2com.fragments.NoDataFragment;
import com.task.test.testtask2com.fragments.TableWeatherFragment;
import com.task.test.testtask2com.interfaces.LoadListener;
import com.task.test.testtask2com.logic.WeatherHolder;

public class MainActivity extends AppCompatActivity implements LoadListener {

    private int mFragmentNum;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WeatherHolder.getInstance().setDelegate(this);
        if(null == savedInstanceState){
            setFragment(0);
            mFragmentNum = 0;
        }

        Button updateBtn = (Button)findViewById(R.id.update_btn_id);
        if(null != updateBtn){
            updateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProgressDialog = new ProgressDialog(v.getContext());
                    mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    mProgressDialog.setMessage(v.getContext().getString(R.string.loading_text));
                    mProgressDialog.show();
                    WeatherHolder.getInstance().loadWeather(v.getContext());
                }
            });
        }
    }

    private void setFragment(int fragmentNum){
        Fragment objFragment;
        mFragmentNum = fragmentNum;
        switch (fragmentNum) {
            case 0:
                objFragment = new NoDataFragment();
                break;
            case 1:
                objFragment = new TableWeatherFragment();
                break;
            default:
                objFragment = new NoDataFragment();
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, objFragment);
        ft.commit();
    }

    public void isLoad(boolean isNoError){
        mProgressDialog.dismiss();
        if(isNoError){
            if(mFragmentNum == 0)
                setFragment(1);
            else{
                TableWeatherFragment tableWeatherFragment =
                        (TableWeatherFragment)getSupportFragmentManager()
                                .findFragmentById(R.id.fragment_container);
                if(null != tableWeatherFragment)
                    tableWeatherFragment.updateWeather();
                else Log.d("myLog", "tableFragment not found");
            }
        }
    }
}
