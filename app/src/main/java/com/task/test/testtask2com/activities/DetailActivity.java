package com.task.test.testtask2com.activities;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.TextView;

import com.task.test.testtask2com.R;
import com.task.test.testtask2com.logic.WeatherHolder;
import com.task.test.testtask2com.logic.WeatherItem;

import java.util.Locale;

/**
 * Created by user on 25.12.2015.
 * activity display ditail
 */
public class DetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        int positionInList = getIntent().getIntExtra("element_position", 0);
        WeatherItem currentItem = WeatherHolder.getInstance().getWeatherItems()[positionInList];

        ((TextView) findViewById(R.id.date_view)).setText(android.text.format.DateFormat
                .format("dd-MM-yy hh", currentItem.getDate().getTime()));

        String celsius = getString(R.string.celsius);
        String min = getString(R.string.min_text);
        String max = getString(R.string.max_text);
                ((TextView) findViewById(R.id.temperature_view)).setText(String.format(
                "%d" + celsius, currentItem.getMiddleTemperature()));
        ((TextView) findViewById(R.id.min_max_temp_view)).setText(String.format(
                min + ": %d" + celsius + ", " + max + ": %d" + celsius,
                currentItem.getMinTemperature(), currentItem.getMaxTemperature()));

        String feelLikeStr = getString(R.string.feels_like_text);
                ((TextView) findViewById(R.id.feels_view)).setText(
                String.format(feelLikeStr + ": %d" + celsius, currentItem.getMiddleHeat()));
        ((TextView) findViewById(R.id.feels_min_max_view)).setText(String.format(
                min + ": %d" + celsius + ", " + max + ": %d" + celsius,
                currentItem.getMinHeat(), currentItem.getMaxHeat()));

        ((TextView) findViewById(R.id.precipitation_cloudiness_view)).setText(String.format(
                getString(R.string.precipitation) + ": %s; " + getString(R.string.cloudiness)
                        + ": %s", currentItem.getPrecipitationString(getBaseContext()),
                currentItem.getCloudinessString(getBaseContext())));

        String mmPressure = getString(R.string.mmPressure);
        ((TextView) findViewById(R.id.pressure_view)).setText(
                String.format(getString(R.string.pressure_text) + ": %d" + mmPressure
                        , currentItem.getMiddlePressure()));
        ((TextView) findViewById(R.id.pressure_min_max_view)).setText(String.format(
                min + ": %d" + mmPressure + ", " + max + " : %d" + mmPressure,
                currentItem.getMinPressure(), currentItem.getMaxPressure()));

        ((TextView) findViewById(R.id.wet_view)).setText(
                String.format(getString(R.string.rel_wet_text) + ": %d%%",
                        currentItem.getMiddleWet()));
        ((TextView) findViewById(R.id.wet_min_max_view)).setText(String.format(
                min + ": %d%%, " + max + ": %d%%", currentItem.getMinRelWet(),
                currentItem.getMaxRelWet()));

        String speed = getString(R.string.wind_speed);
        ((TextView) findViewById(R.id.wind_view)).setText(String.format(
                getString(R.string.wind_text)+ ": " + currentItem.getWindDirection(getBaseContext())
                        + "  %d" + speed, currentItem.getMiddleWind()));
        ((TextView) findViewById(R.id.wind_min_max_view)).setText(String.format(
                min + ": %d" + speed + ", " + max + ": %d" + speed, currentItem.getMinWind(),
                currentItem.getMaxWind()));


    }
}
