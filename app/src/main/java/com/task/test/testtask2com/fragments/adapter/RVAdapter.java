package com.task.test.testtask2com.fragments.adapter;

/**
 * Created by user on 23.12.2015.
 * adapter for TableWeatherFragment
 */
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.task.test.testtask2com.R;
import com.task.test.testtask2com.activities.DetailActivity;
import com.task.test.testtask2com.logic.WeatherHolder;
import com.task.test.testtask2com.logic.WeatherItem;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.TimeoutException;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ViewHolder>{

    private WeatherItem[] mDataSet;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mDateView;
        public TextView mTemperatureView;
        public TextView mCloudiness;
        public TextView mPrecipitation;
        public TextView mFeelingView;
        public TextView mWindDirectionView;
        public TextView mWindValueView;
        public TextView mPressureView;
        public TextView mWetView;
        public ViewHolder(View v) {
            super(v);
            mDateView = (TextView)itemView.findViewById(R.id.time_view_id);
            mTemperatureView = (TextView) itemView.findViewById(R.id.temperature_value);
            mCloudiness = (TextView)itemView.findViewById(R.id.cloudiness_view);
            mPrecipitation = (TextView)itemView.findViewById(R.id.precipitation_view);
            mFeelingView = (TextView)itemView.findViewById(R.id.heat_view_value);
            mWindDirectionView = (TextView)itemView.findViewById(R.id.wind_view_direction);
            mWindValueView = (TextView)itemView.findViewById(R.id.wind_view_value);
            mPressureView = (TextView)itemView.findViewById(R.id.pressure_view_value);
            mWetView = (TextView)itemView.findViewById(R.id.rel_wet_view_value);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){
            int position = getLayoutPosition();
            Log.d("myLog", "--" + String.valueOf(position) + "--");

            Intent elemIntent = new Intent(view.getContext(), DetailActivity.class);
            elemIntent.putExtra("element_position", position);
            view.getContext().startActivity(elemIntent);
        }
    }

    public void update(){
        System.arraycopy(WeatherHolder.getInstance().getWeatherItems(), 0, mDataSet, 0, 4);
    }

    public RVAdapter(WeatherItem[] myDataSet) {
        mDataSet = myDataSet;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RVAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_card, parent, false);
        // set the view's size, margins, paddings and layout parameters
        mContext = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WeatherItem currItem = mDataSet[position];
        holder.mTemperatureView.setText(String.format(
                "%d" + mContext.getString(R.string.celsius), currItem.getMiddleTemperature()));
        holder.mCloudiness.setText(currItem.getCloudinessString(mContext));
        holder.mPrecipitation.setText(currItem.getPrecipitationString(mContext));
        holder.mFeelingView.setText(String.format(
                "%d" + mContext.getString(R.string.celsius), currItem.getMiddleHeat()));
        holder.mWindDirectionView.setText(currItem.getWindDirection(mContext));
        holder.mWindValueView.setText(String.format(
                "%d" + mContext.getString(R.string.wind_speed), currItem.getMiddleWind()));
        holder.mPressureView.setText(String.format(
                "%d" + mContext.getString(R.string.mmPressure), currItem.getMiddlePressure()));
        holder.mWetView.setText(String.format("%d %%", currItem.getMiddleWet()));
        holder.mDateView.setText(currItem.getTime(mContext));
    }

    @Override
    public int getItemCount() {
        return mDataSet.length;
    }
}
