package com.task.test.testtask2com.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.task.test.testtask2com.R;

/**
 * Created by user on 23.12.2015.
 */
public class NoDataFragment extends Fragment {

    private View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Log.d("myLog", "onCreate NoDataFragment");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("myLog", "onCreateView NoDataFragment");
        rootView = inflater.inflate(R.layout.fragment_no_data, container, false);
        return rootView;
    }
    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        Log.d("myLog", "onSaveInstanceState NoDataFragment");
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onPause(){
        Log.d("myLog", "onPause NoDataFragment");
        super.onPause();
    }

    @Override
    public void onStop(){
        Log.d("myLog", "onStop NoDataFragment");
        super.onStop();
    }

    @Override
    public void onDestroy(){
        Log.d("myLog", "onDestroy NoDataFragment");
        super.onDestroy();
    }
}
