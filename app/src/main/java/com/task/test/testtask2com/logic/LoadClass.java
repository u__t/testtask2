package com.task.test.testtask2com.logic;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by user on 24.12.2015.
 * for make async connect
 */
public class LoadClass extends AsyncTask<Void, String, WeatherItem[]> {

    private Context mContext;

    LoadClass(Context _context){
        mContext = _context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected WeatherItem[] doInBackground(Void... params){
        //ArrayList<WeatherItem> weatherItems = new ArrayList<WeatherItem>();
        WeatherItem weatherItems[] = new WeatherItem[4];
        String url_str = "http://informer.gismeteo.ru/xml/29634_1.xml";
        try{
            URL url = new URL(url_str);

            URLConnection conn = url.openConnection();
            final int TIMEOUT_VALUE = 5000;
            conn.setConnectTimeout(TIMEOUT_VALUE);
            conn.setReadTimeout(TIMEOUT_VALUE);

            XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser mParser = xmlFactoryObject.newPullParser();
            mParser.setInput(conn.getInputStream(), null);




            int weatherItemCount = -1;
            while (mParser.getEventType() != XmlPullParser.END_DOCUMENT){
                switch (mParser.getEventType()){
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        switch (mParser.getName()){
                            case "FORECAST":{
                                weatherItemCount++;
                                WeatherItem weatherItem = new WeatherItem();
                                weatherItems[weatherItemCount] = weatherItem;

                                Calendar date = new GregorianCalendar(
                                        Integer.parseInt(mParser.getAttributeValue(2)),
                                        Integer.parseInt(mParser.getAttributeValue(1)) - 1,
                                        Integer.parseInt(mParser.getAttributeValue(0)),
                                        Integer.parseInt(mParser.getAttributeValue(3)), 0, 0);

                                weatherItems[weatherItemCount].setDate(date);

                            }break;
                            case "PHENOMENA":{
                                weatherItems[weatherItemCount].setCloudiness(
                                        Integer.parseInt(mParser.getAttributeValue(0)));
                                weatherItems[weatherItemCount].setPrecipitation(
                                        Integer.parseInt(mParser.getAttributeValue(1)));
                            }break;
                            case "PRESSURE":{
                                weatherItems[weatherItemCount].setMaxPressure(
                                        Integer.parseInt(mParser.getAttributeValue(0)));
                                weatherItems[weatherItemCount].setMinPressure(
                                        Integer.parseInt(mParser.getAttributeValue(1)));
                            }break;
                            case "TEMPERATURE":{
                                weatherItems[weatherItemCount].setMaxTemperature(
                                        Integer.parseInt(mParser.getAttributeValue(0)));
                                weatherItems[weatherItemCount].setMinTemperature(
                                        Integer.parseInt(mParser.getAttributeValue(1)));
                            }break;
                            case "WIND":{
                                weatherItems[weatherItemCount].setMaxWind(
                                        Integer.parseInt(mParser.getAttributeValue(0)));
                                weatherItems[weatherItemCount].setMinWind(
                                        Integer.parseInt(mParser.getAttributeValue(1)));
                                weatherItems[weatherItemCount].setWindDirection(
                                        Integer.parseInt(mParser.getAttributeValue(2)));
                            }break;
                            case "RELWET":{
                                weatherItems[weatherItemCount].setMaxRelWet(
                                        Integer.parseInt(mParser.getAttributeValue(0)));
                                weatherItems[weatherItemCount].setMinRelWet(
                                        Integer.parseInt(mParser.getAttributeValue(1)));
                            }break;
                            case "HEAT":{
                                weatherItems[weatherItemCount].setMaxHeat(
                                        Integer.parseInt(mParser.getAttributeValue(0)));
                                weatherItems[weatherItemCount].setMinHeat(
                                        Integer.parseInt(mParser.getAttributeValue(1)));
                            }break;
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                    case XmlPullParser.TEXT:break;
                    default:break;
                }
                mParser.next();
            }
        }catch (MalformedURLException e){
            Log.e("myLog", "URL exception" + e.toString());
            publishProgress("URL exception" + e.toString());
            weatherItems = null;
        }catch (IOException e) {
            Log.e("myLog", "URLConnection exception" + e.toString());
            publishProgress("URLConnection exception" + e.toString());
            weatherItems = null;
        }catch (XmlPullParserException e) {
            Log.e("myLog", "XmlPullParser exception" + e.toString());
            publishProgress("XmlPullParser exception" + e.toString());
            weatherItems = null;
        }
        return weatherItems;
    }

    @Override
    protected void onProgressUpdate(String... progress) {
        Toast.makeText(mContext, progress[0], Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostExecute(WeatherItem[] result) {
        super.onPostExecute(result);
        WeatherHolder.getInstance().loadFinish(result);
    }
}
