package com.task.test.testtask2com.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.task.test.testtask2com.R;
import com.task.test.testtask2com.fragments.adapter.RVAdapter;
import com.task.test.testtask2com.logic.WeatherHolder;


/**
 * Created by user on 23.12.2015.
 * fragment with weather
 */
public class TableWeatherFragment extends Fragment {

    private View rootView;
    private RecyclerView mRecyclerView;
    private RVAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Log.d("myLog", "onCreate TableWeatherFragment");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("myLog", "onCreateView TableWeatherFragment");
        rootView = inflater.inflate(R.layout.fragment_table_weather, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_id);


        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        mAdapter = new RVAdapter(WeatherHolder.getInstance().getWeatherItems());
        mRecyclerView.setAdapter(mAdapter);


        return rootView;
    }
    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        Log.d("myLog", "onSaveInstanceState TableWeatherFragment");
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onPause(){
        Log.d("myLog", "onPause TableWeatherFragment");
        super.onPause();
    }

    @Override
    public void onStop(){
        Log.d("myLog", "onStop TableWeatherFragment");
        super.onStop();
    }

    @Override
    public void onDestroy(){
        Log.d("myLog", "onDestroy TableWeatherFragment");
        super.onDestroy();
    }

    public void updateWeather(){
        //todo обновить список
        Log.d("myLog", "--update--");
        mAdapter.update();
    }
}
