package com.task.test.testtask2com.logic;

import android.content.Context;
import android.util.Log;

import com.task.test.testtask2com.R;

import java.util.Calendar;

/**
 * Created by user on 24.12.2015.
 *
 */

public class WeatherItem {
    private Calendar mDate;//day month year hour
    private int mCloudiness;
    private int mPrecipitation;
    private int mMaxPressure;
    private int mMinPressure;
    private int mMaxTemperature;
    private int mMinTemperature;
    private int mMaxWind;
    private int mMinWind;
    private int mWindDirection;
    private int mMaxRelWet;
    private int mMinRelWet;
    private int mMaxHeat;
    private int mMinHeat;

    public WeatherItem(){

    }

    public void setDate(Calendar mDate) {
        this.mDate = mDate;
    }

    public Calendar getDate() {
        return mDate;
    }

    public int getCloudiness() {
        return mCloudiness;
    }

    public void setCloudiness(int mCloudiness) {
        this.mCloudiness = mCloudiness;
    }

    public String getCloudinessString(Context _context){
        return (getCloudiness() == 3)?
                _context.getString(R.string.cloudy_text):
                _context.getString(R.string.dont_know_text);
    }

    public void setPrecipitation(int mPrecipitation) {
        this.mPrecipitation = mPrecipitation;
    }

    public int getPrecipitation() {
        return mPrecipitation;
    }

    public String getPrecipitationString(Context _context){
        return (getPrecipitation() == 10)?
                _context.getString(R.string.snow_text):
                _context.getString(R.string.dont_know_text);
    }

    public int getMaxPressure() {
        return mMaxPressure;
    }

    public void setMaxPressure(int mMaxPressure) {
        this.mMaxPressure = mMaxPressure;
    }

    public int getMinPressure() {
        return mMinPressure;
    }

    public void setMinPressure(int mMinPressure) {
        this.mMinPressure = mMinPressure;
    }

    public int getMiddlePressure(){
        return (mMaxPressure + mMinPressure) / 2;
    }

    public int getMaxTemperature() {
        return mMaxTemperature;
    }

    public void setMaxTemperature(int mMaxTemperature) {
        this.mMaxTemperature = mMaxTemperature;
    }

    public int getMinTemperature() {
        return mMinTemperature;
    }

    public void setMinTemperature(int mMinTemperature) {
        this.mMinTemperature = mMinTemperature;
    }

    public int getMiddleTemperature(){
        return (mMaxTemperature + mMinTemperature) / 2;
    }

    public int getMaxWind() {
        return mMaxWind;
    }

    public void setMaxWind(int mMaxWind) {
        this.mMaxWind = mMaxWind;
    }

    public int getMinWind() {
        return mMinWind;
    }

    public void setMinWind(int mMinWind) {
        this.mMinWind = mMinWind;
    }

    public int getMiddleWind(){
        return (mMaxWind + mMinWind) / 2;
    }

    public int getWindDirection() {
        return mWindDirection;
    }

    public void setWindDirection(int mWindDirection) {
        this.mWindDirection = mWindDirection;
    }

    public String getWindDirection(Context mContext){
        String[] mWindDirectionArray = mContext.getResources()
                .getStringArray(R.array.wind_direction_text);
        return mWindDirectionArray[getWindDirection()];
    }

    public int getMaxRelWet() {
        return mMaxRelWet;
    }

    public void setMaxRelWet(int mMaxRelWet) {
        this.mMaxRelWet = mMaxRelWet;
    }

    public int getMinRelWet() {
        return mMinRelWet;
    }

    public void setMinRelWet(int mMinRelWet) {
        this.mMinRelWet = mMinRelWet;
    }

    public int getMiddleWet(){
        return (mMaxRelWet + mMinRelWet) / 2;
    }

    public int getMaxHeat() {
        return mMaxHeat;
    }

    public void setMaxHeat(int mMaxHeat) {
        this.mMaxHeat = mMaxHeat;
    }

    public int getMinHeat() {
        return mMinHeat;
    }

    public void setMinHeat(int mMinHeat) {
        this.mMinHeat = mMinHeat;
    }

    public int getMiddleHeat(){
        return (mMaxHeat + mMinHeat) / 2;
    }

    public String getTime(Context _context){
        String time;
        switch (getDate().get(Calendar.HOUR_OF_DAY)){
            case 3:
                time = _context.getString(R.string.night_text);
                break;
            case 9:
                time = _context.getString(R.string.morning_text);
                break;
            case 15:
                time = _context.getString(R.string.day_text);
                break;
            case 21:
                time = _context.getString(R.string.evening_text);
                break;
            default:
                time = _context.getString(R.string.day_text);
                break;
        }
        return time;
    }

    public String toString(){
        String result = "";
        result = "date = " + mDate.toString() + "\n";
        result += "PHENOMENA: cloudiness=" + String.valueOf(mCloudiness) + " precipitation=" +
                String.valueOf(mPrecipitation) + "\n";
        result += "PRESSURE max=" + String.valueOf(mMaxPressure) + " min=" +
                String.valueOf(mMinPressure) + "\n";
        result += "TEMPERATURE max=" + String.valueOf(mMaxTemperature) + " min=" +
                String.valueOf(mMinTemperature) + "\n";
        result += "WIND max=" + String.valueOf(mMaxWind) + " min=" + String.valueOf(mMinWind)
                + "\n";
        result += "RELWET max=" + String.valueOf(mMaxRelWet) + " min=" + String.valueOf(mMinRelWet)
                + "\n";
        result += "HEAT max=" + String.valueOf(mMaxHeat) + " min=" + String.valueOf(mMinHeat)
                + "\n";
        return result;
    }
}
